package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
/**
 * Greeting is a class used for testing package
 * @author Daria Kurbatova
 * @version 8/29/2022
 */
public class Greeter{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        int test = keyboard.nextInt();
        Random rn = new Random();
        int randomNum = rn.nextInt(10);
        System.out.println(randomNum);
        Utilities ut = new Utilities();
        System.out.println(ut.doubleMe(randomNum));
        
    }
}