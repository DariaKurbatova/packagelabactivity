package secondpackage;
/**
 * Utilites is a class used for testing methods within packages
 * @author Daria Kurbatova
 * @version 8/29/2022
 */
public class Utilities {
    /**
     * Doubles the input value
     * 
     * @param toDouble the value to be doubled
     * @return the value of toDouble times two
     */
    public int doubleMe(int toDouble){
        return toDouble*2;
    }
}
